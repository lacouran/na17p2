**Description du projet : lien du sujet :https://librecours.net/dawa/projets-2020-II/co/adherents.html**

l'objectif de ce projet est de réaliser un outil permettant de gérer les adhérents d'une association.
Une personne peut déposer informations personnelles, certaines étant ou non étant obligatoire .
Les adhesions des différentes personnes sont gérés par des administrateurs . Une adhesion dispose d'un type qui est choisi par l'administrateur . 
Ls administrateurs  ont également la charges des dons  et des groupes .
Un don est fait par une personne a l'association  et doit être controlé par un  administrateurs . 
Afin d'avoir un controle sur les actions des administrateurs le fait d'ajouter un don ,ou un adherent est stockés en précisant qui a réalisé la demarche .
Un groupe est un ensemble de personnes partageant des données , ces données peuvent être un évènement,une URL,des photos,ainsi que le lien vers la page du site concernant le groupe .
La  conservation des données  dans le temps doit être intégrés dans la gestion de cette outil .Pour réaliser cela on créé un système d'archivage .
 L'aspect privatif ( accès des données, informations personnelles ) est mis en valeur dans le cahier des charges, on fera donc attention a ces problematiques .



**Liste des objets qui devront être gérés dans la base de données**

*     Adherent
*     Donateur 
*     Administrateur
*     Adhesion
*     Don
*     Groupe
*     Evenement
*     Archive 
*     Photo




* Personne :  

    

* [ ]     possède un login unique

* [ ]     peut posséder des informations complémentaires (nom,prénom,photos, numéro de téléphone ,,adresses)

* [ ]     Doit pouvoir être exporté en format Vcard



Le mot de passe n'est pas stockés en clair, mais salé  et encrypté .




* Donateur:  

* [ ]     Effectue un don

* [ ]     Est géré par un administrateur 





* Administrateur : 


* [ ]     Valide les adhesions aux groupes
* [ ]     Valide les dons
* [ ]     Valide les adherents 





Contrainte concernant Donateur,adherent et administrateur . Ces 3 classes heritent de Personne et son exclusif, un administrateur n'est donc pas un adherent ni un donateur . 
Un adherent peut cependant effectuer un don .


* Adherent 

* [ ]     Géré par un administrateur
* [ ]     Possède une adhesion
* [ ]     Date d'entrée
* [ ]     Date de fin



* Adhesion :  

    Correspond a une architecture d'adhesion (or ,bronze...),( honoraire, membre a vie, payant )




* Don  : 
    

* [ ]     Réalisé par un administrateur
* [ ]     Concerne un donateur 
* [ ]     Dispose d'un montant
* [ ]     Dispose du type de monnaie du montant



Contrainte : si un don est effectué par un donateur, il n'est pas effectuer par un adherent 


* Groupe : 
    Un groupe est constitués d'au moins une personne, si il n'y a plus de membres dans le groupe celui ci est supprimé,ainsi que les photos e le concernant ,les évènements ne concernant que ce groupe sont également supprimés .
    

* [ ]     Lien d'une URL
* [ ]     Lien vers des photos 
* [ ]     Lien page web


* Photos : 

* [ ]     Titre
* [ ]     URL
* [ ]     Est possédé par un groupe ou une personne 



Contrainte : si une photo est possédé par un groupe elle ne peut pas correspondre a une personne 


* Archivage : 
    Doit être stocké dans une table a part  ,et contient les informations d'anciens membres .
    
        
    
    

**    Hypothèse pour la modélisation:**

    *    H1 : on ne gère qu'une seule association, cependant  on fera attention a la réalisation pour que la base de donnée soit portable si on désire gérer un ensemble d'association .
        H2 : la cohérence des données est importante,donc pas d'imbrication 
        H3 :Le type d'adhesion est volatile, et peut facilement être modifié 
        H4 : Deux évènements ne peuvent pas avoir le même nom 
        H5 : Deux evenements peuvent être partagés entre plusieurs groupes
        H5 : Si une personne est supprimé; son archivage est toujours présent .
        H6 : Deux personnes peuvent avoir le même prénom/nom si ils sont renseignés 
        H7 :L'archivage ne peut pas être gérer par des vues , car stocké dans un endroit a part de la BDD.
        H8 : les photos ne sont pas partagés, même si ils sont identiques 
        H9 : Un admin n'est ni un adherent ,ni un donateur *

        
**Fonctionnalité  qui nécessite des adaptations côté base de donnée  :**

* [ ]     page de connexion : Vu sur personne avec accès au hash du  mot de passe et au login .
* [ ]     Vu expiration : Vu sur les adherents dont l'adhesion  s'expire d'ici X mois .
* [ ]     Vu log : Permet d'obtenir toute les actions effectués par un admin .

            

**Niveau d'accès :** 
    Il y a deux niveaux d'accès dans notre application : admin et utilisateur . 
    Les utilisateurs n'ont accès qu'a leurs propre informations ,les informations public des autres utilisateurs( y compris l'archivage ) et l'historique de leurs propre don . Les admins ont accès  a toute les informations  , et également le droit de modifier . Il faudra donc faire attention d'un point de vu applicatif a l'élevation de privilège .